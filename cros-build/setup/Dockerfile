FROM debian:buster

ARG DEBIAN_FRONTEND=noninteractive

# enable SSL / HTTPS support
RUN apt-get update && apt-get install --no-install-recommends -y \
    apt-transport-https \
    ca-certificates

# install dependencies for cros-sdk
RUN apt-get update && apt-get install --no-install-recommends -y \
    curl \
    git \
    gnupg \
    lvm2 \
    procps \
    python-pkg-resources \
    python3.6 \
    sudo \
    thin-provisioning-tools \
    xz-utils

# add cros-build user with sudo
RUN \
  mkdir -p /home/cros-build && \
  useradd cros-build -d /home/cros-build && \
  chown cros-build: -R /home/cros-build && \
  adduser cros-build sudo && \
  echo "cros-build ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers
USER cros-build

# get depot_tools and add it to the path
RUN \
  git clone \
  --depth=1 \
  https://chromium.googlesource.com/chromium/tools/depot_tools.git \
  /home/cros-build/depot_tools
ENV PATH=/home/cros-build/depot_tools:$PATH

# create the working directory where to download the Chromium OS files
WORKDIR /home/cros-build/chromiumos
RUN sudo chown cros-build: -R /home/cros-build/chromiumos

# get the source code repo manifest and download the "firmware" part of the SDK
ARG cros_sdk_branch=master
RUN \
  repo init \
    -u https://chromium.googlesource.com/chromiumos/manifest.git \
    -b $cros_sdk_branch \
    -g firmware \
    --depth=1 && \
  repo sync -j4

# install helper scripts and do extra setup
RUN git config --global user.name cros-sdk-firmware
RUN git config --global user.email cros-sdk-firmware@collabora.com
ARG cros_device
ENV CROS_DEVICE=$cros_device
ADD --chown=cros-build create-chroot.sh create-chroot.sh
ADD --chown=cros-build $cros_device.* src/scripts/
